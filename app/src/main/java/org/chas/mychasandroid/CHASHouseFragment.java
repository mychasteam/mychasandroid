package org.chas.mychasandroid;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.chas.mychasandroid.Data.Post;
import org.chas.mychasandroid.RecyclerViewFeed.PostsAdapter;
import org.chas.mychasandroid.helper.AppController;
import org.chas.mychasandroid.helper.SQLite;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class CHASHouseFragment extends Fragment implements PostsAdapter.ClickHandler{


private String TAG = CHASHouseFragment.class.getSimpleName();
    private ProgressDialog pDialog;

    // URL to get feed JSON
    private static String URL_DATA = "http://soc-web-liv-10.napier.ac.uk/login-sys/JsonRetrievals/data.php";
    private static String URL_json = "http://soc-web-liv-10.napier.ac.uk/login-sys/JsonRetrievals/results.json";

    private PostsAdapter mAdapter;


    public static final String NAME_HOME = "Home";
    public static final String NAME_ROBIN_HOUSE = "Robin House";
    public static final String NAME_RACHEL_HOUSE = "Rachel House";
    public static final String NAME_CHAS_AT_HOME = "CHAS at home";


    private RecyclerView recyclerView;
    private String houseName;
    private SQLite db;
    List<Post> posts = new ArrayList<>();
    public CHASHouseFragment() {
        // Required empty public constructor
    }
    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);

        Cache cache = AppController.getInstance().getRequestQueue().getCache();

        Cache.Entry entry = cache.get(URL_json);
        if (entry != null) {
            // fetch the data from cache
            try {
                String data = new String(entry.data, "UTF-8");
                try {
                    JSONObject obj = new JSONObject(data.toString());
                    JSONArray arr = obj.getJSONArray("results");
                    parseJsonFeed(arr);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {
            // making fresh volley request and getting json
            String tag_string_req = "req_data";

            pDialog.setMessage("Please wait...");
            showDialog();

            final StringRequest strReq = new StringRequest(Request.Method.GET,
                    URL_DATA, new Response.Listener<String>() {

                public void onResponse(String response) {
                    Log.d(TAG, "Data Response: " + response.toString());

                    try {
                        JSONArray results=new JSONArray(response);
                        parseJsonFeed(results);
                        boolean error = true;
                        // Check for error node in json
                        if (!error) {

                            parseJsonFeed((results));
                            Log.d(TAG, "Data Response: " + response.toString());
                            hideDialog();
                        } else {

                            hideDialog();
                        }
                    } catch (JSONException e) {
                        // JSON error
                        e.printStackTrace();
                        Toast.makeText(getActivity().getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Error: " + error.getMessage());
                    Toast.makeText(getActivity().getApplicationContext(),
                            error.getMessage(), Toast.LENGTH_LONG).show();
                    hideDialog();

                }
            }) {

            };

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GoToHospiceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CHASHouseFragment newInstance(String houseName) {
        CHASHouseFragment fragment = new CHASHouseFragment();
        fragment.houseName = houseName;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chas_house, container, false);

        ImageView backgroundImageView = (ImageView) view.findViewById(R.id.chas_house_background);
        if (houseName.equals(NAME_RACHEL_HOUSE)) {
            backgroundImageView.setImageResource(R.drawable.rachel_house_background);
        } else if (houseName.equals(NAME_ROBIN_HOUSE)) {
            backgroundImageView.setImageResource(R.drawable.robin_house_background);
        } else if (houseName.equals(NAME_HOME)) {
            backgroundImageView.setImageResource(R.drawable.home_background);
        } else if (houseName.equals(NAME_CHAS_AT_HOME)) {
            backgroundImageView.setImageResource(R.drawable.home_background);
        }

        recyclerView = (RecyclerView) view.findViewById(R.id.chas_house_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return view;
    }
    private void parseJsonFeed(JSONArray response) {

        try {
            for (int i = 0; i <response.length(); i++) {

                JSONObject feedObj = response.getJSONObject(i);

                Post item = new Post();
                item.setId(feedObj.getInt("ID"));
                item.setTitle(feedObj.getString("Title"));
                // Image might be null sometimes
                String image = feedObj.isNull("ImageLink") ? null : feedObj
                        .getString("ImageLink");
                item.setImage(image);
                item.setText(feedObj.getString("Article"));
                item.setDate(feedObj.getString("Date"));
                String housetype =feedObj.getString("HouseType");
               // db.addArticle(feedObj.getString("ID"), item.getTitle(), item.getText(), item.getImage(), item.getDate(), item.getHouseType());
                if (houseName.equals("Home")) {
                    posts.add(new Post(item.getId(), item.getTitle(), item.getText(), item.getImage(), item.getDate(), item.getHouseType()));
                }
                else {
                    if(housetype.equalsIgnoreCase(houseName)){
                        posts.add(new Post(item.getId(), item.getTitle(), item.getText(), item.getImage(), item.getDate(), item.getHouseType()));
                    }

                }
            }
            mAdapter = new PostsAdapter(getActivity(), this, posts);
            recyclerView.setAdapter(mAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onClick(View v, int position) {


    }

    @Override
    public void goToPostDetailActivity(Post post) {
        if(post != null) {
            Intent i = new Intent(getActivity(), PostDetailsActivity.class);
            i.putExtra(Post.PARCEL_TAG, post);
            startActivity(i);
        }
    }

    @Override
    public void onLongClick(View v, int position) {
        Log.d("FEO15", "Long Clicked item : " + position);
    }
}

