package org.chas.mychasandroid.Data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Andrew on 08/02/2017.
 */

public class Post implements Parcelable{
    private int type;
    private int id;
    private String title, text, image, date,housetype;

    public static final int TYPE_DEFAULT = 0;
    public static final int TYPE_IMAGE = 1;

    public static final String PARCEL_TAG = "parceltagpost";

    public Post() {
    }


    public Post(int id, String title, String text, String image, String date,String housetype) {
        super();

        this.id = id;
        this.title = title;
        this.text = text;
        this.date = date;
        this.image = image;
        this.housetype=housetype;
        if(image != null && !image.isEmpty()) {
            this.type = TYPE_IMAGE;
        } else {
            this.type = TYPE_DEFAULT;
        }

    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
    public String getHouseType() {
        return housetype;
    }

    public void setHouseType(String housetype) {
        this.housetype = housetype;
    }

    // Parcelling part
    public Post(Parcel in){
        String[] data = new String[5];

        in.readStringArray(data);
        // the order needs to be the same as in writeToParcel() method
        this.title = data[0];
        this.text = data[1];
        this.image = data[2];
        this.date = data[3];
        this.housetype = data[4];
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {this.title,
                this.text,
                this.image,
                this.date,
                this.housetype});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Post createFromParcel(Parcel in) {
            return new Post(in);
        }

        public Post[] newArray(int size) {
            return new Post[size];
        }
    };

}