package org.chas.mychasandroid;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by feorin on 2017-02-08.
 */

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();
    }

    private OnDateAndTimeSelected onDateAndTimeSelected;
    private int flag;



    public DatePickerFragment() {}

    public static DatePickerFragment getInstance(OnDateAndTimeSelected onDateAndTimeSelected, int flag) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setOnDateAndTimeSelected(onDateAndTimeSelected);
        datePickerFragment.setFlag(flag);

        return datePickerFragment;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public void setOnDateAndTimeSelected(OnDateAndTimeSelected onDateAndTimeSelected) {
        this.onDateAndTimeSelected = onDateAndTimeSelected;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        Dialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);

        // Create a new instance of DatePickerDialog and return it
        return dialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        onDateAndTimeSelected.goToTimePickerDialog(flag, year, month + 1, dayOfMonth);
    }
}