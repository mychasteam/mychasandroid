package org.chas.mychasandroid;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.chas.mychasandroid.faq.FaqAdapter;
import org.chas.mychasandroid.faq.FaqAnswer;
import org.chas.mychasandroid.faq.FaqQuestion;
import org.chas.mychasandroid.helper.AppController;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FaqFragment extends Fragment{
    private String TAG = FaqFragment.class.getSimpleName();
    private ProgressDialog pDialog;
    public static final String NAME_FAQ = "Faq";
    private static String URL_DATA = "http://soc-web-liv-10.napier.ac.uk/login-sys/JsonRetrievals/faqData.php";
    public FaqAdapter faqAdapter;
    private RecyclerView recyclerView;
    List<FaqQuestion> answers = new ArrayList<>();
    public FaqFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }



    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GoToHospiceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FaqFragment newInstance() {
        FaqFragment fragment = new FaqFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_faq, container, false);
         recyclerView = (RecyclerView) view.findViewById(R.id.faq_recycler_view);
      LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        RecyclerView.ItemAnimator animator = recyclerView.getItemAnimator();
        if (animator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        String tag_string_req = "req_data";

        pDialog.setMessage("Please wait...");
        showDialog();
        final StringRequest strReq = new StringRequest(Request.Method.GET,
                URL_DATA, new Response.Listener<String>() {

            public void onResponse(String response) {
                Log.d(TAG, "Data Response: " + response.toString());

                try {
                    JSONArray results=new JSONArray(response);
                    parseJsonFeed(results);
                    boolean error = true;
                    // Check for error node in json
                    if (!error) {
                        parseJsonFeed((results));
                        Log.d(TAG, "Data Response: " + response.toString());
                        hideDialog();
                    } else {


                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();

            }
        }) {

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

        faqAdapter = new FaqAdapter(answers);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(faqAdapter);


        return view;
    }



    public static List<FaqAnswer> makeAnswer(String answer) {
        List<FaqAnswer> answers = new ArrayList<>();
        answers.add(new FaqAnswer(answer));
        return answers;
    }

    public static FaqQuestion makeQuestion(String question, String answer) {
        return new FaqQuestion(question, makeAnswer(answer));
    }

    private void parseJsonFeed(JSONArray response) {
        List<FaqQuestion> questions = new ArrayList<>();
        try {
            for (int i = 0; i <response.length(); i++) {

                JSONObject feedObj = response.getJSONObject(i);

                    questions.add(makeQuestion(feedObj.getString("Question") , feedObj.getString("Answer")));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        hideDialog();
        answers=questions;
        faqAdapter = new FaqAdapter(answers);
        recyclerView.setAdapter(faqAdapter);

    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }



}