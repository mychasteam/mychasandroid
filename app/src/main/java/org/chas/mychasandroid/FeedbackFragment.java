package org.chas.mychasandroid;


import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class FeedbackFragment extends Fragment implements View.OnClickListener {

    private ProgressDialog pDialog;

    String email="kons.stefanos@gmail.com";
    String user = "droidapptest94@gmail.com";
    String pass = "androidtest"; //enter password
    String body="";
    String subject="";
    EditText mFeed;
    EditText mSubject;
    EditText name;
    RatingBar mBar;


    public FeedbackFragment() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feedback,container,false);
        Button sendmail=(Button) view.findViewById(R.id.btn_SendFeedback);
        mFeed   = (EditText)view.findViewById(R.id.feedback);
        mSubject=(EditText)view.findViewById(R.id.subject);
        name= (EditText)view.findViewById(R.id.name);
        mBar = (RatingBar) view.findViewById(R.id.ratingBar);
        sendmail.setOnClickListener(this);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();

        int yellowR = Color.red(Color.YELLOW);
        int yellowG = Color.green(Color.YELLOW);
        int yellowB = Color.blue(Color.YELLOW);

        stars.getDrawable(0).setColorFilter(Color.argb(100, yellowR, yellowG, yellowB), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.argb(100, yellowR, yellowG, yellowB), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

        return view;
    }

    @Override
    public void onClick(View view){

    switch (view.getId()){
        case R.id.btn_SendFeedback:
            if (name.getText().length()<=0)
                Toast.makeText(getContext(),"Please enter your name!",Toast.LENGTH_SHORT).show();
            else if (mSubject.getText().length()<=0)
                Toast.makeText(getContext(),"Please enter a subject!",Toast.LENGTH_SHORT).show();
            else if (mFeed.getText().length()<=0)
                Toast.makeText(getContext(),"Please enter your feedback!",Toast.LENGTH_SHORT).show();
            else {
                pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pDialog.setMessage("Sending..");
                pDialog.setIndeterminate(true);
                pDialog.setCancelable(false);
                pDialog.show();
                body = "From: " + name.getText().toString() + "\n \n" + mFeed.getText().toString()
                        + "Rating: " + mBar.getRating() + "/5";
                subject = "Feedback - "+ mSubject.getText().toString();
                final MailSender mail = new MailSender(user, pass, email, body, subject);
                try {
                    mail.send();
                    hideDialog();
                    mFeed.setText("");
                    mSubject.setText("");
                    name.setText("");
                    Toast.makeText(getContext(),"Feedback sent!",Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    hideDialog();
                }
            }
    }
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }




}
