package org.chas.mychasandroid;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class GoToHospiceFragment extends Fragment  implements OnDateAndTimeSelected, AdapterView.OnItemSelectedListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public static int DATE_AND_TIME_FROM = 0;
    public static int DATE_AND_TIME_TO = 1;

    public static String DATE_PICKER_TAG = "datePicker";
    public static String TIME_PICKER_TAG = "timePicker";

    private Button buttonDateFrom;
    private Button buttonDateTo;

    private Spinner spinnerSelectHospice;
    private EditText editTextRequirements;
    private EditText fullname;
    private Button buttonSubmit;

    String email="kons.stefanos@gmail.com";
    String user = "droidapptest94@gmail.com";
    String pass = "androidtest"; //enter password
    String body="";
    String subject="";


    public GoToHospiceFragment() {
        // Required empty public constructor
    }



    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GoToHospiceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GoToHospiceFragment newInstance(String param1, String param2) {
        GoToHospiceFragment fragment = new GoToHospiceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_go_to_hospice, container, false);

        fullname=(EditText) view.findViewById(R.id.fullname);
        editTextRequirements=(EditText)view.findViewById(R.id.additionalDetails);
        buttonDateFrom = (Button) view.findViewById(R.id.buttonDateFrom);
        buttonDateTo = (Button) view.findViewById(R.id.buttonDateTo);

        buttonDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(DATE_AND_TIME_FROM);
            }
        });

        buttonDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(DATE_AND_TIME_TO);
            }
        });

        if (savedInstanceState != null) {
            DatePickerFragment datePickerFrag = (DatePickerFragment) getActivity().getSupportFragmentManager()
                    .findFragmentByTag(DATE_PICKER_TAG);

            TimePickerFragment timePickerFrag = (TimePickerFragment) getActivity().getSupportFragmentManager()
                    .findFragmentByTag(TIME_PICKER_TAG);

            if (datePickerFrag != null) {
                datePickerFrag.setOnDateAndTimeSelected(this);
            }

            if (timePickerFrag != null) {
                timePickerFrag.setOnDateAndTimeSelected(this);
            }

        }
        spinnerSelectHospice = (Spinner) view.findViewById(R.id.spinnerSelectHospice);
        spinnerSelectHospice.setOnItemSelectedListener(this);

        List<String> list = new ArrayList<>();
        list.add("Rachel House");
        list.add("Robin House");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSelectHospice.setAdapter(dataAdapter);

        buttonSubmit = (Button) view.findViewById(R.id.buttonSubmit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitRequest();
            }
        });

        return view;
    }

    public void submitRequest(){
        String nameOfHouse = (String) spinnerSelectHospice.getSelectedItem();
        String dateFrom = buttonDateFrom.getText().toString();
        String dateTo = buttonDateTo.getText().toString();
        if (fullname.getText().length()<=0)
            Toast.makeText(getContext(),"Please enter your name!",Toast.LENGTH_SHORT).show();
        else if (dateFrom.length()<=0)
            Toast.makeText(getContext(),"Please enter your check in date!",Toast.LENGTH_SHORT).show();
        else if (dateTo.length()<=0)
            Toast.makeText(getContext(),"Please enter your check out date!",Toast.LENGTH_SHORT).show();
        else if (editTextRequirements.getText().length()<=0)
            Toast.makeText(getContext(),"Please enter your email or phone number!",Toast.LENGTH_SHORT).show();
        else {
            body =  "From: " + fullname.getText().toString() + "\n \n" + "Visiting: "
                    + nameOfHouse+ "\n \n"
                    + "Additional Details: " + editTextRequirements.getText().toString()
                    + "\n \n" + "From "+ dateFrom  + "\n \n" + "To: "+ dateTo;
            subject = "Go to hospice  ";
            final MailSender mail = new MailSender(user, pass, email, body, subject);
            try {
                mail.send();
                fullname.setText("");
                buttonDateFrom.setText("");
                buttonDateTo.setText("");
            } catch (Exception e) {
                e.printStackTrace();

            }
        }

        Log.d("FEO1", "----- submitRequest -----");
        Log.d("FEO1", "nameOfHouse: " + nameOfHouse);
        Log.d("FEO1", "dateFrom: " + dateFrom);
        Log.d("FEO1", "dateTo: " + dateTo);

    }


    public void showDatePickerDialog(int flag) {
        DialogFragment newFragment = DatePickerFragment.getInstance(this, flag);
        newFragment.show(getActivity().getSupportFragmentManager(), DATE_PICKER_TAG);
    }

    @Override
    public void goToTimePickerDialog(int flag, int year, int month, int day) {
        DialogFragment newFragment = TimePickerFragment.getInstance(this, flag, year, month, day);
        newFragment.show(getActivity().getSupportFragmentManager(), TIME_PICKER_TAG);
    }

    @Override
    public void goBackToDatePickerDialog(int flag) {
        DialogFragment newFragment = DatePickerFragment.getInstance(this, flag);
        newFragment.show(getActivity().getSupportFragmentManager(), DATE_PICKER_TAG);
    }

    public String getFormatedDateAndTime(int year, int month, int day, int hour, int minute) {
        return "" + year + "/" + month + "/" + day + " " + hour + ":" + minute;
    }


    @Override
    public void dateAndTimeAccepted(int flag, int year, int month, int day, int hour, int minute) {
        if(flag == DATE_AND_TIME_FROM) {
            Log.d("FEO1", "----- dateAndTimeAccepted FROM");
            buttonDateFrom.setText(getFormatedDateAndTime(year, month, day, hour, minute));
        } else if(flag == DATE_AND_TIME_TO) {
            Log.d("FEO1", "----- dateAndTimeAccepted TO");
            buttonDateTo.setText(getFormatedDateAndTime(year, month, day, hour, minute));
        } else {
            Log.d("FEO1", "----- dateAndTimeAccepted  ------- ERROR");
        }

        Log.d("FEO1", "year : " + year);
        Log.d("FEO1", "month : " + month);
        Log.d("FEO1", "day : " + day);
        Log.d("FEO1", "hour : " + hour);
        Log.d("FEO1", "minute : " + minute);

    }

    //spinner callbacks
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
