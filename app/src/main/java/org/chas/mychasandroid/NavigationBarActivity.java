package org.chas.mychasandroid;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class NavigationBarActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_bar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setTitle("Home");
        CHASHouseFragment fragment = CHASHouseFragment.newInstance(CHASHouseFragment.NAME_HOME);
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame,fragment,CHASHouseFragment.NAME_HOME);
        fragmentTransaction.commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.homeFragement) {
            setTitle("Home");
            CHASHouseFragment fragment = CHASHouseFragment.newInstance(CHASHouseFragment.NAME_HOME);
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame,fragment,CHASHouseFragment.NAME_HOME);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        } else if (id == R.id.rachelFragment) {
            setTitle("Rachel House");
            CHASHouseFragment fragment = CHASHouseFragment.newInstance(CHASHouseFragment.NAME_RACHEL_HOUSE);
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame,fragment,CHASHouseFragment.NAME_RACHEL_HOUSE);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
        else if (id == R.id.robinhouse_fragment) {
            setTitle("Robin House");
            CHASHouseFragment fragment = CHASHouseFragment.newInstance(CHASHouseFragment.NAME_ROBIN_HOUSE);
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame,fragment,CHASHouseFragment.NAME_ROBIN_HOUSE);
            fragmentTransaction.commit();
        }
        else if (id == R.id.chasAtHome) {
            setTitle("CHAS at home");
            CHASHouseFragment fragment = CHASHouseFragment.newInstance(CHASHouseFragment.NAME_CHAS_AT_HOME);
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame,fragment,CHASHouseFragment.NAME_CHAS_AT_HOME);
            fragmentTransaction.commit();
        }
        else if (id == R.id.feedback_fragment) {
            setTitle("Feedback");
            FeedbackFragment fragment = new FeedbackFragment();
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame,fragment,"Feedback");
            fragmentTransaction.commit();
        }
        else if (id == R.id.requests_fragment) {
            setTitle("Requests");
            RequestsFragment fragment = new RequestsFragment();
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame,fragment,"Requests");
            fragmentTransaction.commit();
        }
        else if (id == R.id.go_to_hospice_fragment) {
            setTitle("Go to hospice");
            GoToHospiceFragment fragment = new GoToHospiceFragment();
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame,fragment,"Go to hospice");
            fragmentTransaction.commit();
        }
        else if (id == R.id.faq_fragment) {
            setTitle("FAQ");
            FaqFragment fragment = new FaqFragment();
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame,fragment,"FAQ");
            fragmentTransaction.commit();
        }
        else if (id == R.id.about_fragment) {
            setTitle("About CHAS");
            SlidingTabsBasicFragment fragment = new SlidingTabsBasicFragment();
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame,fragment);
            fragmentTransaction.commit();
        }
        else if (id == R.id.donate_fragment) {
            setTitle("Donate");
            DonateFragment fragment = new DonateFragment();
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame,fragment, DonateFragment.NAME_DONATE);
            fragmentTransaction.commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
