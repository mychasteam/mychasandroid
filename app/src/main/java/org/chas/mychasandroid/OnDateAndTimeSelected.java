package org.chas.mychasandroid;

/**
 * Created by feorin on 2017-02-08.
 */

public interface OnDateAndTimeSelected {
    void goToTimePickerDialog(int flag, int year, int month, int day);
    void dateAndTimeAccepted(int flag, int year, int month, int day, int hour, int minute);
    void goBackToDatePickerDialog(int flag);
}
