package org.chas.mychasandroid;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.chas.mychasandroid.Data.Post;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class PostDetailsActivity extends AppCompatActivity {

    private TextView title, date, text;
    private ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);

        title = (TextView) findViewById(R.id.postDetailsTextViewPostTitle);
        text = (TextView) findViewById(R.id.postDetailsTextViewPostText);
        date = (TextView) findViewById(R.id.postDetailsTextViewPostDate);
        imageView = (ImageView) findViewById(R.id.postDetailsImageViewPost);

        Bundle data = getIntent().getExtras();
        Post post = (Post) data.getParcelable(Post.PARCEL_TAG);

        if(post != null) {

            title.setText(post.getTitle());
            date.setText(post.getDate());
            text.setText(post.getText());

            String postImage = post.getImage();
            if (postImage != null && !postImage.isEmpty()) {
                Picasso.with(this).load(postImage).into(imageView);
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            long time = 0;
            try {
                time = sdf.parse(post.getDate()).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long now = System.currentTimeMillis();
            CharSequence ago =
                    DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);

            date.setText(ago);

        }

    }
}
