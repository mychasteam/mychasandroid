package org.chas.mychasandroid.RecyclerViewFeed;


import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.squareup.picasso.Picasso;

import org.chas.mychasandroid.Data.Post;
import org.chas.mychasandroid.R;
import org.chas.mychasandroid.helper.AppController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

public class PostsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Post> postList;
    private LayoutInflater inflater;
    private ClickHandler clickHandler;
    private Context context;

    private static final int MAX_CHAR_COUNT = 250;
    private static final String CONTINUE_READING = "Continue Reading";

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public List<Post> getPostList() {
        return postList;
    }

    public PostsAdapter(Context context, ClickHandler clickHandler, List<Post> list) {
        this.clickHandler = clickHandler;
        this.postList = list;
        this.context = context;
    }

    public interface ClickHandler {
        void onClick(View v, int position);

        void onLongClick(View v, int position);

        void goToPostDetailActivity(Post post);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;

        if (viewType == Post.TYPE_IMAGE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_image_row, parent, false);
            viewHolder = new ImageViewHolder(view);
        } else { // TYPE_DEFAULT
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_default_row, parent, false);
            viewHolder = new DefaultViewHolder(view);
        }
//        setupClickableViewHolder(view, viewHolder);

        return viewHolder;
    }

    public void setupClickableViewHolder(final View v, final RecyclerView.ViewHolder viewHolder) {
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickHandler.onClick(v, viewHolder.getAdapterPosition());

            }
        });
        v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                clickHandler.onLongClick(v, viewHolder.getAdapterPosition());
                return true;
            }
        });
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Post post = postList.get(position);

        if (post != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getDefault());
            long time = 0;
            try {
                time = sdf.parse(post.getDate()).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long now = System.currentTimeMillis();
            CharSequence ago =
                    DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);

            TextView text = null;

            switch (post.getType()) {
                case Post.TYPE_DEFAULT:
                    text = ((DefaultViewHolder) holder).text;
                    ((DefaultViewHolder) holder).title.setText(post.getTitle());
                    ((DefaultViewHolder) holder).date.setText(post.getDate());
                    ((DefaultViewHolder) holder).text.setText(post.getText());
                    ((DefaultViewHolder) holder).date.setText((ago));
                    break;

                case Post.TYPE_IMAGE:
                    text = ((ImageViewHolder) holder).text;
                    ((ImageViewHolder) holder).title.setText(post.getTitle());
                    ((ImageViewHolder) holder).date.setText(post.getDate());
                    ((ImageViewHolder) holder).text.setText(post.getText());
                    ((ImageViewHolder) holder).date.setText((ago));
                    ImageView imageView = ((ImageViewHolder) holder).imageView;
                    Picasso.with(context).load(post.getImage()).into(imageView);
            }

            if (text != null && post.getText().length() > MAX_CHAR_COUNT) {

                String textCut = post.getText();
                textCut = textCut.substring(0, MAX_CHAR_COUNT);

                // find last white space
                int lastWhiteSpace = textCut.lastIndexOf(" ");
                textCut = textCut.substring(0, lastWhiteSpace);

                textCut = textCut + "... " + CONTINUE_READING;

                SpannableString ss = new SpannableString(textCut);

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        clickHandler.goToPostDetailActivity(post);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setColor(Color.BLUE);
                        ds.setUnderlineText(false);
                    }
                };
                ss.setSpan(clickableSpan, textCut.lastIndexOf(CONTINUE_READING), textCut.lastIndexOf(CONTINUE_READING) + CONTINUE_READING.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                text.setText(ss);
                text.setMovementMethod(LinkMovementMethod.getInstance());

            }


        }
    }


    @Override
    public int getItemCount() {
        if (postList == null)
            return 0;
        return postList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (postList != null) {
            Post post = postList.get(position);
            if (post != null) {
                return post.getType();
            }
        }
        return 0;
    }

    public static class DefaultViewHolder extends RecyclerView.ViewHolder {
        public TextView title, date, text, url;

        public DefaultViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.textViewPostTitle);
            text = (TextView) itemView.findViewById(R.id.textViewPostText);
            date = (TextView) itemView.findViewById(R.id.textViewPostDate);
        }
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        public TextView title, date, text;
        public ImageView imageView;

        public ImageViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.textViewPostTitle);
            text = (TextView) itemView.findViewById(R.id.textViewPostText);
            date = (TextView) itemView.findViewById(R.id.textViewPostDate);
            imageView = (ImageView) itemView.findViewById(R.id.imageViewPost);
        }
    }
}