package org.chas.mychasandroid;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class RequestsFragment extends Fragment implements View.OnClickListener  {


    String email="kons.stefanos@gmail.com";
    String user = "droidapptest94@gmail.com";
    String pass = "androidtest"; //enter password
    String body="";
    String subject="";
    String useremail="";
    EditText mFeed;
    EditText mSubject;
    EditText name;
    EditText mEmail;

    public RequestsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_requests,container,false);

        Button sendmail=(Button) view.findViewById(R.id.btn_SendRequest);
        sendmail.setOnClickListener(this);
        mFeed   = (EditText)view.findViewById(R.id.request);
        mSubject=(EditText)view.findViewById(R.id.subject);
        name= (EditText)view.findViewById(R.id.name);
        mEmail= (EditText)view.findViewById(R.id.email);

        return view;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_SendRequest:
                if (mEmail.getText().length()<=0)
                    useremail="Email not stated";
                else
                    useremail=mEmail.getText().toString();
                if (name.getText().length()<=0)
                    Toast.makeText(getContext(),"Please enter your name!",Toast.LENGTH_SHORT).show();
                else if (mSubject.getText().length()<=0)
                    Toast.makeText(getContext(),"Please enter a subject!",Toast.LENGTH_SHORT).show();
                else if (mFeed.getText().length()<=0)
                    Toast.makeText(getContext(),"Please enter your request!",Toast.LENGTH_SHORT).show();
                else {
                    body = "From: " + name.getText().toString() +"\n \n"+ "Email: "+ useremail + "\n \n"
                            + "Request: "+ mFeed.getText().toString();

                    subject ="Request - " + mSubject.getText().toString();
                    final MailSender mail = new MailSender(user, pass, email, body, subject);
                    try {
                        mail.send();
                        mFeed.setText("");
                        mSubject.setText("");
                        name.setText("");
                        mEmail.setText("");
                        Toast.makeText(getContext(),"Request sent!",Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                }


        }
    }

