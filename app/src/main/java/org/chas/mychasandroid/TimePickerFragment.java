package org.chas.mychasandroid;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by feorin on 2017-02-08.
 */

public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    private OnDateAndTimeSelected onDateAndTimeSelected;
    private int flag;
    private int year;
    private int month;
    private int day;

    public void setYear(int year) {
        this.year = year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public TimePickerFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();
    }



    public static TimePickerFragment getInstance(OnDateAndTimeSelected onDateAndTimeSelected, int flag, int year, int month, int day) {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setOnDateAndTimeSelected(onDateAndTimeSelected);
        timePickerFragment.setFlag(flag);
        timePickerFragment.setYear(year);
        timePickerFragment.setMonth(month);
        timePickerFragment.setDay(day);

        return timePickerFragment;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public void setOnDateAndTimeSelected(OnDateAndTimeSelected onDateAndTimeSelected) {
        this.onDateAndTimeSelected = onDateAndTimeSelected;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        TimePickerDialog dialog = new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));

        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    onDateAndTimeSelected.goBackToDatePickerDialog(flag);
                }
            }
        });

        // Create a new instance of TimePickerDialog and return it
        return dialog;
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        onDateAndTimeSelected.dateAndTimeAccepted(flag, year, month, day, hourOfDay, minute);
    }
}