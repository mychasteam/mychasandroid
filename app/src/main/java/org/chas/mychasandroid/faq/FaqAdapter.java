package org.chas.mychasandroid.faq;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import org.chas.mychasandroid.R;

import java.util.List;

/**
 * Created by feorin on 2017-03-11.
 */

public class FaqAdapter extends ExpandableRecyclerViewAdapter<FaqQuestionViewHolder, FaqAnswerViewHolder> {

    public FaqAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public FaqQuestionViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.faq_item_question, parent, false);
        return new FaqQuestionViewHolder(view);
    }

    @Override
    public FaqAnswerViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.faq_item_answer, parent, false);
        return new FaqAnswerViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(FaqAnswerViewHolder holder, int flatPosition,
                                      ExpandableGroup group, int childIndex) {

        final FaqAnswer artist = ((FaqQuestion) group).getItems().get(childIndex);
        holder.setAnswerText(artist.getText());
    }

    @Override
    public void onBindGroupViewHolder(FaqQuestionViewHolder holder, int flatPosition,
                                      ExpandableGroup group) {
        holder.setQuestionText(group);
    }



}
