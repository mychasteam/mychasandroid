package org.chas.mychasandroid.faq;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by feorin on 2017-03-11.
 */

public class FaqAnswer implements Parcelable {

    private String text;

    public FaqAnswer(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    protected FaqAnswer(Parcel in) {
        text = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FaqAnswer> CREATOR = new Creator<FaqAnswer>() {
        @Override
        public FaqAnswer createFromParcel(Parcel in) {
            return new FaqAnswer(in);
        }

        @Override
        public FaqAnswer[] newArray(int size) {
            return new FaqAnswer[size];
        }
    };
}