package org.chas.mychasandroid.faq;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import org.chas.mychasandroid.R;

/**
 * Created by feorin on 2017-03-11.
 */

public class FaqAnswerViewHolder extends ChildViewHolder{

    private TextView childTextView;

    public FaqAnswerViewHolder(View itemView) {
        super(itemView);
        childTextView = (TextView) itemView.findViewById(R.id.faq_item_answer_text);
    }

    public void setAnswerText(String name) {
        childTextView.setText(name);
    }

}
