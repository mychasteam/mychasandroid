package org.chas.mychasandroid.faq;

import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import org.chas.mychasandroid.R;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

/**
 * Created by feorin on 2017-03-11.
 */

public class FaqQuestionViewHolder extends GroupViewHolder{

    private TextView questionText;
    private ImageView arrow;

    public FaqQuestionViewHolder(View itemView) {
        super(itemView);
        questionText = (TextView) itemView.findViewById(R.id.faq_item_question_text);
        arrow = (ImageView) itemView.findViewById(R.id.faq_item_question_arrow);
    }

    public void setQuestionText(ExpandableGroup group) {
        questionText.setText(group.getTitle());
    }

    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }

}
