package org.chas.mychasandroid.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by tomat_000 on 14/03/2017.
 */

public class SQLite extends SQLiteOpenHelper {

    private  static final String TAG = SQLite.class.getSimpleName();

    private  static final int DATABASE_VERSION = 1;

    private  static final String DATABASE_NAME = "chasdatabase";

    private  static  final  String TABLE_FEED ="newsfeed";

    private static final String KEY_ID = "ID";
    private static final String KEY_TITLE ="Title";
    private static  final String KEY_ARTICLE ="Article";
    private  static  final String KEY_DATE ="Date";
    private  static  final String KEY_IMAGE ="ImageLink";
    private  static  final String KEY_URL ="Url";




    public SQLite(Context context) {super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_FEED_TABLE = "CREATE TABLE " + TABLE_FEED + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"+ KEY_TITLE + " TEXT,"
                + KEY_ARTICLE + " TEXT," +  KEY_IMAGE + " TEXT," +  KEY_URL + " TEXT," +  KEY_DATE + " TEXT"  + ")";
        db.execSQL(CREATE_FEED_TABLE);

        Log.d(TAG,"Database Table created");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS" + TABLE_FEED);

        onCreate(db);

    }
    public void addArticle( String ID,String Title,String Article, String Image, String URL, String Date){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID,ID);
        values.put(KEY_TITLE,Title);
        values.put(KEY_ARTICLE,Article);
        values.put(KEY_DATE,Date);
        values.put(KEY_DATE,Date);
        values.put(KEY_IMAGE,Image);
        values.put(KEY_URL,URL);

        long id = db.insert(TABLE_FEED,null,values);
        db.close();

        Log.d(TAG, "New article added in sqlite " + id);


    }

    public HashMap<String,String> getArticleDetails(){
        HashMap<String, String> Articles = new HashMap<String, String>();
        String selectQuery = "SELECT * FROM " + TABLE_FEED;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);

        cursor.moveToFirst();
        if (cursor.getCount() >0){
            Articles.put("ID",cursor.getString(1));
            Articles.put("Title",cursor.getString(2));
            Articles.put("Article",cursor.getString(3));
            Articles.put("ImageLink",cursor.getString(4));
            Articles.put("Url",cursor.getString(5));
            Articles.put("Date",cursor.getString(6));

        }
        cursor.close();
        db.close();
        Log.d(TAG,"Fetching user from Sqlite:" + Articles.toString());

        return Articles;


    }

    public  void  deleteArticles(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_FEED, null,null);
        db.close();
        Log.d(TAG,"Deleted all articles from sql lite");

    }
}
